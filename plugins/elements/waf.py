#  Copyright (C) 2018 Codethink Ltd.
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Martin Blanchard <martin.blanchard@codethink.co.uk>

"""
waf - Waf build element
===========================
This is a :mod:`BuildElement <buildstream.buildelement>` implementation for
using `Waf <https://waf.io/>`_ build scripts.

You will often want to pass additional arguments to ``waf``. This should
be done on a per-element basis by setting the ``waf-local`` variable.  Here is
an example:

.. code:: yaml

   variables:
     waf-local: |
       --debug

If you want to pass extra options to ``waf`` for every element in your
project, set the ``waf-global`` variable in your project.conf file. Here is
an example of that:

.. code:: yaml

   elements:
     waf:
       variables:
         waf-global: |
           --strict

Here is the default configuration for the ``waf`` element in full:

  .. literalinclude:: ../../../buildstream/plugins/elements/waf.yaml
     :language: yaml
"""

from buildstream import BuildElement


# Element implementation for the 'waf' kind.
class WafElement(BuildElement):
    pass


# Plugin entry point
def setup():
    return WafElement
